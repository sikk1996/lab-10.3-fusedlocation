package net.schjem.lab10_3_fusedlocation;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.pm.PackageManager;

import android.graphics.Color;
import android.location.Location;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // Constants
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private static final String CHANNEL_TIME_IS_UP = "fused_channel";
    private static final String CHANNEL_DISTANCE_REACHED = "fused_channel2";
    private static final int TIME_IS_UP = 1;
    private static final int DISTANCE_REACHED = 0;
    private static final Long msToSecond = 1000L;
    private static final Long secToMinute = 60L;
    private static final String TRACKING_LOCATION_KEY = "tracking_location";

    //User input
    Long timeToCountDownInMinutes;
    Long timeToCountDownInMs;
    int distanceToGo;
	float distanceTraveled;


	// Views
    private Button mLocationButton;
    private EditText et_Time;
    private EditText et_Distance;
    private ProgressBar progressBar;
    private ProgressBar progressBarDistance;

    // Location classes
    private boolean mTrackingLocation;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private Location locationStart = null;
    private Location locationNow;
    ArrayList<Double> avgSpeedList = new ArrayList<>();

    //Notificaton
    NotificationManagerCompat notificationManager;



	//CountDownTimer
    CountDownTimer timer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


	    mLocationButton = findViewById(R.id.btn_Track);
        et_Time = findViewById(R.id.et_Time);
        et_Distance = findViewById(R.id.et_Distance);
        progressBar = findViewById(R.id.progressBar);
        progressBarDistance = findViewById(R.id.progressBar2);

        //Create notification channel and initialize notificationManager
	    createNotificationChannel();
	    notificationManager = NotificationManagerCompat.from(this);


        // Initialize the FusedLocationClient.
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        // Restore the state if the activity is recreated.
        if (savedInstanceState != null) {
            mTrackingLocation = savedInstanceState.getBoolean(
                    TRACKING_LOCATION_KEY);
        }

        // Set the listener for the location button.
        mLocationButton.setOnClickListener(new View.OnClickListener() {
            /**
             * Toggle the tracking state.
             * @param v The track location button.
             */
            @Override
            public void onClick(View v) {
	            timeToCountDownInMinutes =  Long.parseLong(et_Time.getText().toString());
                timeToCountDownInMs = secToMinute * msToSecond * timeToCountDownInMinutes;
                distanceToGo = Integer.parseInt(et_Distance.getText().toString());
                progressBar.setMax(Integer.parseInt(timeToCountDownInMs / 1000 + ""));
                progressBarDistance.setMax(distanceToGo);
                if (!mTrackingLocation) {
                    startTrackingLocation();
                } else {
                    stopTrackingLocation();
                }
            }
        });

        // Initialize the location callbacks.
        mLocationCallback = new LocationCallback() {
            /**
             * This is the callback that is triggered when the
             * FusedLocationClient updates your location.
             * @param locationResult The result containing the device location.
             */
            @Override
            public void onLocationResult(LocationResult locationResult) {

                if (mTrackingLocation) {
                    if (locationStart == null)
                        locationStart = locationResult.getLastLocation();
                    locationNow = locationResult.getLastLocation();
	                distanceTraveled = locationStart.distanceTo(locationNow);
	                //mLocationTextView.setText("You have traveled " + distanceTraveled + " metres");
	                progressBarDistance.setProgress((int)distanceTraveled);
	                avgSpeedList.add(Double.parseDouble(locationNow.getSpeed()+""));
                    if (distanceTraveled >= distanceToGo) {
                        winCondition(DISTANCE_REACHED);
                    }
                }
            }
        };
    }
	private void createNotificationChannel() {
		// Create the NotificationChannel, but only on API 26+ because
		// the NotificationChannel class is new and not in the support library
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			CharSequence name = getString(R.string.channel_name);
			String description = getString(R.string.channel_description);
			int importance = NotificationManager.IMPORTANCE_HIGH;
			List<NotificationChannel> list = new ArrayList<>();

			AudioAttributes attributes = new AudioAttributes.Builder()
					.setUsage(AudioAttributes.USAGE_NOTIFICATION)
					.build();


			//Time channel
			NotificationChannel channel = new NotificationChannel(CHANNEL_TIME_IS_UP, name, importance);
			channel.setDescription(description);
			channel.enableLights(true);
			channel.setLightColor(Color.RED);
			Uri uri=Uri.parse("android.resource://"+getPackageName()+"/raw/sad_trombone");
			channel.setSound(uri,attributes);
			list.add(channel);

			//Distance channel
			NotificationChannel channel2 = new NotificationChannel(CHANNEL_DISTANCE_REACHED, name, importance);
			channel2.setDescription(description);
			channel2.enableLights(true);
			channel.setLightColor(Color.GREEN);
			Uri uri2=Uri.parse("android.resource://"+getPackageName()+"/raw/win");
			channel2.setSound(uri2,attributes);
			list.add(channel2);




			// Register the channel with the system; you can't change the importance
			// or other notification behaviors after this
			NotificationManager notificationManager = getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannels(list);
		}
	}

    private void winCondition(int i) {
    	if (mTrackingLocation){
		    int notificationId = 1;
		    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL_DISTANCE_REACHED)
				    .setContentTitle("")
				    .setContentText("")
				    .setPriority(NotificationCompat.PRIORITY_HIGH);
		    if (i == DISTANCE_REACHED) {
			    //Distanse nådd
			    Double avgSpeed = 0.0;
			    for (Double speed: avgSpeedList) {
						avgSpeed+=speed;
			    }
			    avgSpeed = avgSpeed/avgSpeedList.size();
			    builder.setContentTitle("Distance reached! ");
			    builder.setContentText((int)distanceTraveled+" metres traveled! Altitude: "+(locationNow.getAltitude()-locationStart.getAltitude())+". Avg speed: "+ avgSpeed);
			    builder.setSmallIcon(R.drawable.finish);
			    builder.setChannelId(CHANNEL_DISTANCE_REACHED);
			    notificationManager.notify(notificationId, builder.build());
		    } else {
			    //Tiden gikk ut
			    builder.setContentTitle("Time is up!");
			    builder.setContentText("You used "+timeToCountDownInMinutes+" minutes, and only went "+distanceTraveled+" of "+distanceToGo+" meters");
			    builder.setSmallIcon(R.drawable.stopwatch);
			    builder.setChannelId(CHANNEL_TIME_IS_UP);
			    notificationManager.notify(notificationId, builder.build());
		    }
		    //Reset
		    stopTrackingLocation();
	    }
    }


    private void startTrackingLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                            {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        } else {
            mTrackingLocation = true;
            mFusedLocationClient.requestLocationUpdates
                    (getLocationRequest(),
                            mLocationCallback,
                            null /* Looper */);
	        avgSpeedList = new ArrayList<>();
            mLocationButton.setText(R.string.stop_tracking_location);

            //Countdown start
            if (et_Time.getText().toString() == "") et_Time.setText("5");
            timer = new CountDownTimer(timeToCountDownInMs, msToSecond) {
                @Override
                public void onTick(long millisUntilFinished) {
                    progressBar.setProgress(Integer.parseInt((timeToCountDownInMs - millisUntilFinished) / 1000 + ""));
                }

                @Override
                public void onFinish() {
                    progressBar.setProgress(Integer.parseInt(timeToCountDownInMs / 1000 + ""));
                    winCondition(TIME_IS_UP);
                }
            };
            timer.start();
        }
    }

    private void stopTrackingLocation() {
        if (mTrackingLocation) {
            mTrackingLocation = false;
            mLocationButton.setText(R.string.start_tracking_location);

            timer.cancel();
            locationStart = null;
            locationNow = null;
	        timeToCountDownInMinutes=0L;
	        timeToCountDownInMs=0L;
	        distanceToGo=0;
	        distanceTraveled=0;
        }
    }

    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }


    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(TRACKING_LOCATION_KEY, mTrackingLocation);
        super.onSaveInstanceState(outState);
    }

    /**
     * Callback that is invoked when the user responds to the permissions
     * dialog.
     *
     * @param requestCode  Request code representing the permission request
     *                     issued by the app.
     * @param permissions  An array that contains the permissions that were
     *                     requested.
     * @param grantResults An array with the results of the request for each
     *                     permission requested.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:

                // If the permission is granted, get the location, otherwise,
                // show a Toast
                if (grantResults.length > 0
                        && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED) {
                    startTrackingLocation();
                } else {
                    Toast.makeText(this,
                            R.string.location_permission_denied,
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onPause() {
        if (mTrackingLocation) {
            stopTrackingLocation();
            mTrackingLocation = true;
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (mTrackingLocation) {
            startTrackingLocation();
        }
        super.onResume();
    }

}